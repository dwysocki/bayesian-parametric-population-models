from __future__ import division, print_function

def get_percentiles(data, weights=None, axis=0):
    import numpy
    from .. import utils

    percentiles = numpy.array([2.50, 16.0, 50.0, 84.0, 97.5])
    quantiles = percentiles / 100.0

    data_percentiles = utils.quantile(
        data, quantiles, weights=weights,
        axis=axis,
    )

    return {
        "median": data_percentiles[2],
        "1 sigma": (data_percentiles[1], data_percentiles[3]),
        "2 sigma": (data_percentiles[0], data_percentiles[4]),
    }

def plot_percentiles(ax, X, percentiles, color="black", alpha=0.25, label=None):
    for lo, hi in [percentiles["1 sigma"], percentiles["2 sigma"]]:
        ax.fill_between(
            X[1:-1], lo[1:-1], hi[1:-1],
            color=color, alpha=alpha,
        )
    ax.plot(
        X[1:-1], percentiles["median"][1:-1],
        color=color,
        label=label,
    )


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "plot_m1_pm1",
        help="File to store plot of mass probability distribution.",
    )
    parser.add_argument(
        "plot_m1_R_pm1",
        help="File to store plot of mass rate distribution.",
    )
    parser.add_argument(
        "plot_pchi",
        help="File to store plot of spin magnitude probability distributions.",
    )
    parser.add_argument(
        "plot_R_pchi",
        help="File to store plot of spin magnitude rate distributions.",
    )
    parser.add_argument(
        "plot_Pchi_eff",
        help="File to store plot of effective spin cumulative probability "
             "distributions.",
    )
    parser.add_argument(
        "plot_R_Pchi_eff",
        help="File to store plot of effective spin cumulative rate "
             "distributions.",
    )

    parser.add_argument(
        "--n-samples",
        default=1000, type=int,
        help="Number of samples to use in plots.",
    )

    parser.add_argument(
        "--n-kde-samples",
        default=1000, type=int,
        help="Number of samples to draw to compute KDEs.",
    )

    parser.add_argument(
        "--total-mass-max",
        default=100.0, type=float,
        help="Maximum total mass to plot.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--seed",
        type=int,
        help="Seed for random number generator.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import h5py
    import numpy
    import scipy.stats as stats

    from . import prob
    from . import utils
    from ..utils import empirical_distribution_function

    if raw_args is None:
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    import seaborn as sns

    matplotlib.rc("text", usetex=True)

    sns.set_context("talk", font_scale=2, rc={"lines.linewidth": 2.5})
    sns.set_style("ticks")
    sns.set_palette("colorblind")

    rand_state = numpy.random.RandomState(args.seed)

    with h5py.File(args.posteriors, "r") as f:
        metadata = f.attrs
        param_names = prob.param_names
        params = utils.load(f, param_names, metadata)

        log10_rates = params["log_rate"]
        alphas = params["alpha"]
        m_mins = params["m_min"]
        m_maxs = params["m_max"]
        log10_alpha_chi1s = params["log_alpha_chi1"]
        log10_beta_chi1s = params["log_beta_chi1"]
        log10_alpha_chi2s = params["log_alpha_chi2"]
        log10_beta_chi2s = params["log_beta_chi2"]

        m_min = numpy.min(m_mins)
        m_max = numpy.max(m_maxs)
        M_max = args.total_mass_max

        rates = numpy.power(10.0, log10_rates)
        alpha_chi1s = numpy.power(10.0, log10_alpha_chi1s)
        beta_chi1s = numpy.power(10.0, log10_beta_chi1s)
        alpha_chi2s = numpy.power(10.0, log10_alpha_chi2s)
        beta_chi2s = numpy.power(10.0, log10_beta_chi2s)

        (
            rates,
            alphas,
            m_mins, m_maxs,
            alpha_chi1s, beta_chi1s,
            alpha_chi2s, beta_chi2s,
        ) = utils.upcast_scalars((
            rates,
            alphas,
            m_mins, m_maxs,
            alpha_chi1s, beta_chi1s,
            alpha_chi2s, beta_chi2s,
        ))

        if args.rate_logU_to_jeffereys:
            weights = numpy.sqrt(rates)
        else:
            weights = None

        n_posterior = numpy.size(rates)

        m = numpy.linspace(m_min, m_max, args.n_samples)
        chi = numpy.linspace(0.0, 1.0, args.n_samples)
        chi_eff = chi

        m_pm = numpy.empty((n_posterior, args.n_samples), dtype=numpy.float64)
        m_R_pm = numpy.empty_like(m_pm)

        pchi1 = numpy.empty_like(m_pm)
        R_pchi1 = numpy.empty_like(m_pm)
        pchi2 = numpy.empty_like(m_pm)
        R_pchi2 = numpy.empty_like(m_pm)

        Pchi_eff = numpy.empty_like(m_pm)
        R_Pchi_eff = numpy.empty_like(m_pm)

        iterables = zip(
            rates,
            alphas,
            m_mins, m_maxs,
            alpha_chi1s, beta_chi1s,
            alpha_chi2s, beta_chi2s,
        )

        for (
                i,
                (
                    rate,
                    alpha, m_min, m_max,
                    alpha_chi1, beta_chi1, alpha_chi2, beta_chi2
                )
            ) in enumerate(iterables):
            pm = prob.marginal_m1_pdf(m, alpha, m_min, m_max, M_max)

            m_pm[i] = m * pm
            m_R_pm[i] = m * rate * pm

            del pm

            pchi1[i] = prob.marginal_spin_mag_pdf(chi, alpha_chi1, beta_chi1)
            R_pchi1[i] = rate * pchi1[i]

            pchi2[i] = prob.marginal_spin_mag_pdf(chi, alpha_chi2, beta_chi2)
            R_pchi2[i] = rate * pchi2[i]

            m1_samp, m2_samp, chi1_samp, chi2_samp = prob.joint_rvs(
                args.n_kde_samples,
                alpha, m_min, m_max, M_max,
                alpha_chi1, beta_chi1, alpha_chi2, beta_chi2,
                rand_state=rand_state,
            ).T

            chi_eff_samp = (
                (m1_samp*chi1_samp + m2_samp*chi2_samp) /
                (m1_samp + m2_samp)
            )

            Pchi_eff[i] = empirical_distribution_function(chi_eff, chi_eff_samp)
            R_Pchi_eff[i] = rate * Pchi_eff[i]

            # pchi_eff[i] = stats.gaussian_kde(
            #     chi_eff_samp, bw_method="scott",
            # )(chi_eff)
            # R_pchi_eff[i] = rate * pchi_eff[i]

            del chi_eff_samp
            del m1_samp, m2_samp, chi1_samp, chi2_samp


        m_pm_percentile = get_percentiles(m_pm, weights=weights)
        m_R_pm_percentile = get_percentiles(m_R_pm, weights=weights)

        pchi1_percentile = get_percentiles(pchi1, weights=weights)
        R_pchi1_percentile = get_percentiles(R_pchi1, weights=weights)
        pchi2_percentile = get_percentiles(pchi2, weights=weights)
        R_pchi2_percentile = get_percentiles(R_pchi2, weights=weights)

        Pchi_eff_percentile = get_percentiles(Pchi_eff, weights=weights)
        R_Pchi_eff_percentile = get_percentiles(R_Pchi_eff, weights=weights)

        Pchi_eff_marginal = numpy.average(Pchi_eff, weights=weights, axis=0)
        R_Pchi_eff_marginal = numpy.average(R_Pchi_eff, weights=weights, axis=0)

        fig_m_pm, ax_m_pm = plt.subplots()
        fig_R_pm, ax_R_m_pm = plt.subplots()
        fig_pchi, ax_pchi = plt.subplots()
        fig_R_pchi, ax_R_pchi = plt.subplots()
        fig_Pchi_eff, ax_Pchi_eff = plt.subplots()
        fig_R_Pchi_eff, ax_R_Pchi_eff = plt.subplots()

        for ax in [ax_m_pm, ax_R_m_pm]:
            ax.set_xlabel(r"$m_1$ [M$_\odot$]")
            ax.set_xscale("log")
            ax.set_yscale("log")

        for ax in [ax_pchi, ax_R_pchi]:
            ax.set_xlabel(r"$\chi_i$")
            ax.set_yscale("log")

        for ax in [ax_Pchi_eff, ax_R_Pchi_eff]:
            ax.set_xlabel(r"$\chi_{\mathrm{eff}}$")

        ax_m_pm.set_ylabel(
            r"$m_1 \, p(m_1)$"
        )
        ax_R_m_pm.set_ylabel(
            r"$m_1 \, \mathcal{R} \, p(m_1)$ [Gpc$^{-3}$ yr$^{-1}$]"
        )

        ax_pchi.set_ylabel(
            r"$p(\chi_i)$"
        )
        ax_R_pchi.set_ylabel(
            r"$\mathcal{R} \, p(\chi_i)$"
        )

        ax_Pchi_eff.set_ylabel(
            r"$p(< \chi_{\mathrm{eff}})$"
        )
        ax_R_Pchi_eff.set_ylabel(
            r"$\mathcal{R} \, p(< \chi_{\mathrm{eff}})$"
        )

        plot_percentiles(ax_m_pm, m, m_pm_percentile)
        plot_percentiles(ax_R_m_pm, m, m_R_pm_percentile)

        plot_percentiles(ax_pchi, chi, pchi1_percentile, color="#1f77b4")
        plot_percentiles(ax_pchi, chi, pchi2_percentile, color="#2ca02c")

        plot_percentiles(ax_R_pchi, chi, R_pchi1_percentile, color="#1f77b4")
        plot_percentiles(ax_R_pchi, chi, R_pchi2_percentile, color="#2ca02c")

        plot_percentiles(ax_Pchi_eff, chi_eff, Pchi_eff_percentile)
        plot_percentiles(ax_R_Pchi_eff, chi_eff, R_Pchi_eff_percentile)

        ax_Pchi_eff.plot(
            chi_eff, Pchi_eff_marginal,
            color="#1f77b4", linestyle="--",
        )
        ax_R_Pchi_eff.plot(
            chi_eff, R_Pchi_eff_marginal,
            color="#1f77b4", linestyle="--",
        )

        for ax in [ax_m_pm, ax_R_m_pm]:
            ax.set_xlim([m_min, M_max])

        for ax in [ax_pchi, ax_R_pchi, ax_Pchi_eff, ax_R_Pchi_eff]:
            ax.set_xlim([0.0, 1.0])

        for fig in [fig_m_pm, fig_R_pm, fig_Pchi_eff, fig_R_Pchi_eff]:
            fig.tight_layout()

        fig_m_pm.savefig(args.plot_m1_pm1)
        fig_R_pm.savefig(args.plot_m1_R_pm1)

        fig_pchi.savefig(args.plot_pchi)
        fig_R_pchi.savefig(args.plot_R_pchi)

        fig_Pchi_eff.savefig(args.plot_Pchi_eff)
        fig_R_Pchi_eff.savefig(args.plot_R_Pchi_eff)


if __name__ == "__main__":
    import sys
    raw_args = sys.argv[1:]
    sys.exit(_main(raw_args))
