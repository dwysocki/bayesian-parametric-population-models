from __future__ import division, print_function

def get_percentiles(data, weights=None, axis=0):
    import numpy
    from .. import utils

    percentiles = numpy.array([2.50, 16.0, 50.0, 84.0, 97.5])
    quantiles = percentiles / 100.0

    data_percentiles = utils.quantile(
        data, quantiles, weights=weights,
        axis=axis,
    )

    return {
        "median": data_percentiles[2],
        "1 sigma": (data_percentiles[1], data_percentiles[3]),
        "2 sigma": (data_percentiles[0], data_percentiles[4]),
    }

def plot_percentiles(ax, X, percentiles, color="black", alpha=0.25, label=None):
    for lo, hi in [percentiles["1 sigma"], percentiles["2 sigma"]]:
        ax.fill_between(
            X[1:-1], lo[1:-1], hi[1:-1],
            color=color, alpha=alpha,
        )
    ax.plot(
        X[1:-1], percentiles["median"][1:-1],
        color=color,
        label=label,
    )


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "plot_prob",
        help="File to store plot of mass probability distribution.",
    )
    parser.add_argument(
        "plot_rate",
        help="File to store plot of mass rate distribution.",
    )

    parser.add_argument(
        "--n-samples",
        default=1000, type=int,
        help="Number of samples to use in plots.",
    )

    parser.add_argument(
        "--burnin",
        default=0, type=int,
        help="Number of samples to discard at the beginning of the chain.",
    )
    parser.add_argument(
        "--acorr-time",
        default=1, type=int,
        help="Use only one sample per acorr-time.",
    )

    parser.add_argument(
        "--component-mass-min",
        default=5.0, type=float,
        help="Minimum component mass to plot.",
    )
    parser.add_argument(
        "--total-mass-max",
        default=100.0, type=float,
        help="Maximum total mass to plot.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import h5py
    import numpy
    import scipy.stats as stats

    from . import prob
    from . import utils

    if raw_args is None:
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    import seaborn as sns

    matplotlib.rc("text", usetex=True)

    sns.set_context("talk", font_scale=2, rc={"lines.linewidth": 2.5})
    sns.set_style("ticks")
    sns.set_palette("colorblind")

    m_min = args.component_mass_min
    M_max = args.total_mass_max
    m_max = M_max - m_min

    with h5py.File(args.posteriors, "r") as f:
        metadata = f.attrs
        param_names = prob.param_names
        params = utils.load(f, param_names, metadata)

        log10_rates = params["log_rate"]
        alphas = params["alpha"]

        log10_rates, alphas = utils.loop_constants((log10_rates, alphas))

        rates = numpy.power(10.0, log10_rates)

        if args.rate_logU_to_jeffereys:
            weights = numpy.sqrt(rates)
        else:
            weights = None

        n_posterior = max(1, max(numpy.size(log10_rates), numpy.size(alphas)))

        m, dm = numpy.linspace(m_min, m_max, args.n_samples, retstep=True)

        m_pm = numpy.empty((n_posterior, args.n_samples), dtype=numpy.float64)
        m_R_pm = numpy.empty_like(m_pm)

        for i, (rate, alpha) in enumerate(zip(rates, alphas)):

            pm = prob.marginal_pdf(m, alpha, m_min, m_max, M_max)

            m_pm[i] = m * pm
            m_R_pm[i] = m * rate * pm

        m_pm_percentile = get_percentiles(m_pm, weights=weights)
        m_R_pm_percentile = get_percentiles(m_R_pm, weights=weights)

        fig_prob, ax_prob = plt.subplots()
        fig_rate, ax_rate = plt.subplots()

        for ax in [ax_prob, ax_rate]:
            ax.set_xlabel(r"$m_1$ [M$_\odot$]")
            ax.set_xscale("log")
            ax.set_yscale("log")

        ax_prob.set_ylabel(
            r"$m_1 \, p(m_1)$"
        )
        ax_rate.set_ylabel(
            r"$m_1 \, \mathcal{R} \, p(m_1)$ [Gpc$^{-3}$ yr$^{-1}$]"
        )

        plot_percentiles(ax_prob, m, m_pm_percentile)
        plot_percentiles(ax_rate, m, m_R_pm_percentile)

        for ax in [ax_prob, ax_rate]:
            ax.set_xlim([m_min, M_max])

        for fig in [fig_prob, fig_rate]:
            fig.tight_layout()

        fig_prob.savefig(args.plot_prob)
        fig_rate.savefig(args.plot_rate)


if __name__ == "__main__":
    import sys
    raw_args = sys.argv[1:]
    sys.exit(_main(raw_args))
