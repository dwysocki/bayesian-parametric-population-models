import collections
from pop_models.types import Parameters, ParametersSingle

class LRUDict(collections.OrderedDict):
    """
    A dict-like object which only remembers a limited number of elements, using
    "least recently used" caching, i.e., when a new item is inserted, if the
    item count will exceed the max size, the last item which was inserted or
    accessed will be deleted.
    """
    def __init__(self, maxsize: int=128, *args, **kwargs):
        self.__maxsize = maxsize
        super().__init__(*args, **kwargs)

    @property
    def maxsize(self) -> int:
        return self.__maxsize

    def __getitem__(self, key):
        value = super().__getitem__(key)
        self.move_to_end(key)
        return value

    def __setitem__(self, key, value):
        # Store nothing if maxsize is zero
        if self.maxsize == 0:
            return

        super().__setitem__(key, value)
        if len(self) > self.maxsize:
            oldest = next(iter(self))
            del self[oldest]


class ParametersSingleCache(object):
    """
    A dict-like object which maps ParametersSingle objects to values, but only
    remembering a limited number of elements, up to the ``maxsize`` most
    recently used.  Used for caching results for parameters.
    """
    def __init__(self, maxsize: int=128):
        self.__cache = LRUDict(maxsize=maxsize)

    @property
    def maxsize(self) -> int:
        return self.__cache.maxsize

    def _hash_params(self, parameters: ParametersSingle) -> int:
        return hash(frozenset(parameters.items()))

    def __getitem__(self, parameters: ParametersSingle):
        key = self._hash_params(parameters)
        return self.__cache[key]

    def __setitem__(self, parameters: ParametersSingle, value) -> None:
        key = self._hash_params(parameters)
        self.__cache[key] = value

    def __contains__(self, parameters: ParametersSingle) -> bool:
        key = self._hash_params(parameters)
        return key in self.__cache
